import json
from flask import request
from flask_api import FlaskAPI

ASSETS_DATA_FILE = "resources/assets.json"
SCANS_DATA_FILE = "resources/scans.json"
USERS_DATA_FILE = "resources/users.json"
VULNERABILITIES_DATA_FILE = "resources/vulnerabilities.json"

app = FlaskAPI(__name__)


# Load data from file. This could be replaced by getting the data in realtime
def load_json_data(filename):
    with open(filename) as f:
        data = json.loads(f.read())

    return data


# loading nested values for user and assets. (this can be done before filtering,
# which allow more filtering options. To not make this more complex,
# let's leave the filter only by id)
def populate_scan(qs):
    for scan in qs:
        user_id = scan.get("requested_by")
        if user_id:
            Users = load_json_data(USERS_DATA_FILE)
            for user in Users:
                if user_id == user["id"]:
                    scan["requested_by"] = user

        # assuming that the order of the scanned assets does not matter
        if scan.get("assets_scanned"):
            Assets = load_json_data(ASSETS_DATA_FILE)
            scan["assets_scanned"] = [
                a for a in Assets if a["id"] in scan["assets_scanned"]
            ]

    return qs


# loading data here could make it easier but, to simulate a scenario where we
# need to get data when requested and not hardcoded, let's load it inside views
# Assets = load_json_data(ASSETS_DATA_FILE)
# Scans = load_json_data(SCANS_DATA_FILE)
# Users = load_json_data(USERS_DATA_FILE)
# Vulnerabilities = load_json_data(VULNERABILITIES_DATA_FILE)


# Using the same method to show all data or just for a specific user.
@app.route("/users/", methods=["GET"])
def get_users():
    # Simulating a queryset with the JSON, so we can
    # use multiple filters (with some loss in performance)
    queryset = load_json_data(USERS_DATA_FILE)

    if request.args.get("id"):
        queryset = [u for u in queryset if u["id"] == int(request.args.get("id"))]

    if request.args.get("username"):
        queryset = [
            u
            for u in queryset
            if u["username"].lower().find(request.args.get("username").lower()) >= 0
        ]

    if request.args.get("email"):
        queryset = [
            u
            for u in queryset
            if u["email"].lower().find(request.args.get("email").lower()) >= 0
        ]

    # If no params, return the full queryset. This can be changed to
    # return blank instead.
    return queryset


@app.route("/scans/", methods=["GET"])
def get_scans():

    queryset = load_json_data(SCANS_DATA_FILE)

    # Start filtering
    if request.args.get("id"):
        queryset = [s for s in queryset if s["id"] == int(request.args.get("id"))]

    if request.args.get("requested_by"):
        queryset = [
            s
            for s in queryset
            if s["requested_by"] == int(request.args.get("requested_by"))
        ]

    if request.args.get("name"):
        # look for any match of the given name
        queryset = [
            s
            for s in queryset
            if s["name"].lower().find(request.args.get("name").lower()) >= 0
        ]

    if request.args.get("status"):
        # for safety reasons, look for the exact match of status
        # (but ignoring case thought :) )
        queryset = [
            s
            for s in queryset
            if s["status"].lower() == request.args.get("status").lower()
        ]

    if request.args.get("scanner"):
        queryset = [s for s in queryset if request.args.get("scanner") in s["scanners"]]

    if request.args.get("asset_id"):
        queryset = [
            s
            for s in queryset
            if int(request.args.get("asset_id")) in s["assets_scanned"]
        ]

    if request.args.get("fill_nested"):
        queryset = populate_scan(queryset)

    return queryset


@app.route("/assets/", methods=["GET"])
def get_assets():
    queryset = load_json_data(ASSETS_DATA_FILE)

    if request.args.get("id"):
        queryset = [a for a in queryset if a["id"] == int(request.args.get("id"))]
    if request.args.get("name"):
        queryset = [
            a
            for a in queryset
            if a["name"].lower().find(request.args.get("name").lower()) >= 0
        ]

    return queryset


@app.route("/vulnerabilities/", methods=["GET"])
def get_vulnerabilities():
    queryset = load_json_data(VULNERABILITIES_DATA_FILE)

    if request.args.get("id"):
        queryset = [v for v in queryset if v["id"] == int(request.args.get("id"))]
    if request.args.get("from_scan"):
        queryset = [
            v for v in queryset if v["from_scan"] == int(request.args.get("from_scan"))
        ]
    if request.args.get("severity"):
        queryset = [
            v
            for v in queryset
            if v["severity"].lower() == request.args.get("severity").lower()
        ]
    if request.args.get("name"):
        queryset = [
            v
            for v in queryset
            if v["name"].lower().find(request.args.get("name").lower()) >= 0
        ]
    if request.args.get("asset_id"):
        queryset = [
            v
            for v in queryset
            if v["asset_id"] == int(request.args.get("affected_assets"))
        ]

    # assuming that the order of the scanned assets does not matter
    if request.args.get("fill_nested"):
        for vulnerability in queryset:
            scan_id = vulnerability.get("from_scan")
            if scan_id:
                Scans = load_json_data(SCANS_DATA_FILE)

                for scan in populate_scan(Scans):
                    if scan_id == scan["id"]:
                        vulnerability["from_scan"] = scan
                        break

            if vulnerability.get("affected_assets"):
                Assets = load_json_data(ASSETS_DATA_FILE)
                vulnerability["affected_assets"] = [
                    a for a in Assets if a["id"] in vulnerability["affected_assets"]
                ]

    return queryset


if __name__ == "__main__":
    app.run(debug=True)
